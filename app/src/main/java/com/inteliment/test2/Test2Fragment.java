package com.inteliment.test2;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.inteliment.R;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 * {@link Test2Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Test2Fragment extends Fragment implements AdapterView.OnItemSelectedListener {

    FragmentTransaction ft;
    FragmentManager fm;
    private View parentView;
    private TextView txtModeValue;
    Spinner spinner;
    Button btnNavigate;
    Test2Model data;
    FrameLayout spinnerContainer;
    String url = "http://express-it.optusnet.com.au";

    public Test2Fragment() {
        // Required empty public constructor
    }

    /**
     * A new instance of this fragment using the provided parameters.
     * @param text Parameter 1.
     * @return A new instance of fragment Test2Fragment.
     */
    public static Test2Fragment newInstance(String text) {
        Test2Fragment firstFragment = new Test2Fragment();
        Bundle bundle = new Bundle();
        bundle.putString("msg", text);
        firstFragment.setArguments(bundle);
        return firstFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentView = inflater.inflate(R.layout.test2layout, container, false);
        return parentView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Spinner element
        spinnerContainer = (FrameLayout) getView().findViewById(R.id.spinnerContainer);
        spinner = (Spinner) getView().findViewById(R.id.spinner);
        txtModeValue = (TextView) getView().findViewById(R.id.txtModeValue);
        btnNavigate = (Button) getView().findViewById(R.id.btnNavigate);

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);
        RestAdapter radapter = new RestAdapter.Builder().setEndpoint(url).build();
        Test2Interface restInt = radapter.create(Test2Interface.class);
        restInt.getData(new Callback<List<Test2Model>>() {
            @Override
            public void success(List<Test2Model> test2Models, Response response) {
                // Creating adapter for spinner
                ArrayAdapter<Test2Model> dataAdapter = new ArrayAdapter<Test2Model>(getActivity(), android.R.layout.simple_spinner_item, test2Models);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                spinner.setAdapter(dataAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("error", error.getMessage());
            }
        });

        btnNavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerContainer.setVisibility(View.GONE);
                fm = getActivity().getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.add(R.id.mapContainer, MapFragment.newInstance(data.getLocation().getLatitude(), data.getLocation().getLongitude()));
                ft.commit();
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();
        data = (Test2Model) parent.getSelectedItem();
        String strForTxtModeForValue = "";
        if (data.getFromcentral().getCar() != null) {
            strForTxtModeForValue = "Car - " + data.getFromcentral().getCar();
        }

        if (data.getFromcentral().getTrain() != null) {
            strForTxtModeForValue = strForTxtModeForValue + "\n" + "Train - " + data.getFromcentral().getTrain();
        }

        txtModeValue.setText(strForTxtModeForValue);
    }

    public void onNothingSelected(AdapterView<?> arg0) {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
