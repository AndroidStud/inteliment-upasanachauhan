package com.inteliment.test2;

import java.util.List;
import retrofit.Callback;
import retrofit.http.GET;

public interface Test2Interface {
    @GET("/sample.json")
    void getData(Callback<List<Test2Model>> callback);
}
