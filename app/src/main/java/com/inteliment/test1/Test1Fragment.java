package com.inteliment.test1;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.inteliment.R;

/**
 * A simple {@link Fragment} subclass.
 * {@link Test1Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Test1Fragment extends Fragment implements View.OnClickListener{

    private ViewPager pager;
    private Button btn1, btn2, btn3, btn4, btn5, btnRed, btnGreen, btnBlue;
    private TextView txtForScrollViewData;
    private View parentView;
    private TabLayout tabLayout;

    public Test1Fragment() {
        // Required empty public constructor
    }

    /**
     * A new instance of this fragment using the provided parameters.
     * @param text Parameter 1.
     * @return A new instance of fragment FirstFragment.
     */
    public static Test1Fragment newInstance(String text) {
        Test1Fragment firstFragment = new Test1Fragment();
        Bundle bundle = new Bundle();
        bundle.putString("msg", text);
        firstFragment.setArguments(bundle);
        return firstFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentView = inflater.inflate(R.layout.test1_layout, container, false);
        return parentView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        bindWidgets();
        pager.setAdapter(new MyPagerAdapter(getActivity().getSupportFragmentManager()));
        tabLayout.setupWithViewPager(pager);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btnRed.setOnClickListener(this);
        btnGreen.setOnClickListener(this);
        btnBlue.setOnClickListener(this);
    }

    private void bindWidgets() {
        pager = (ViewPager) getView().findViewById(R.id.viewPager);
        btn1 = (Button) getView().findViewById(R.id.btn1);
        btn2 = (Button) getView().findViewById(R.id.btn2);
        btn3 = (Button) getView().findViewById(R.id.btn3);
        btn4 = (Button) getView().findViewById(R.id.btn4);
        btn5 = (Button) getView().findViewById(R.id.btn5);
        txtForScrollViewData = (TextView) getView().findViewById(R.id.txtForScrollViewData);
        btnRed = (Button) getView().findViewById(R.id.btnRed);
        btnGreen = (Button) getView().findViewById(R.id.btnGreen);
        btnBlue = (Button) getView().findViewById(R.id.btnBlue);
        tabLayout = (TabLayout) getView().findViewById(R.id.tabDots);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.btn1:
                txtForScrollViewData.setText(getResources().getString(R.string.item1_clicked));
                break;

            case R.id.btn2:
                txtForScrollViewData.setText(getResources().getString(R.string.item2_clicked));
                break;

            case R.id.btn3:
                txtForScrollViewData.setText(getResources().getString(R.string.item3_clicked));
                break;

            case R.id.btn4:
                txtForScrollViewData.setText(getResources().getString(R.string.item4_clicked));
                break;

            case R.id.btn5:
                txtForScrollViewData.setText(getResources().getString(R.string.item5_clicked));
                break;

            case R.id.btnRed:
                parentView.setBackgroundColor(Color.RED);
                break;

            case R.id.btnGreen:
                parentView.setBackgroundColor(Color.GREEN);
                break;

            case R.id.btnBlue:
                parentView.setBackgroundColor(Color.BLUE);
                break;
        }
    }

    /**
     * MyPagerActivity helps in setting fragments in View pager.
     * Each call to fragment sends the text to be displayed on fragment textview
     */
    private class MyPagerAdapter extends FragmentStatePagerAdapter {

        private MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch(pos) {
                case 0: return FirstFragment.newInstance("FirstFragment, Instance 1");
                case 1: return SecondFragment.newInstance("SecondFragment, Instance 1");
                case 2: return ThirdFragment.newInstance("ThirdFragment, Instance 1");
                case 3: return FourthFragment.newInstance("FourthFragment, Instance 1");
                default: return ThirdFragment.newInstance("ThirdFragment, Default");
            }
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
