package com.inteliment.test1;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.inteliment.R;

/**
 * A simple {@link Fragment} subclass.
 * {@link FourthFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FourthFragment extends Fragment {
    public FourthFragment() {
        // Required empty public constructor
    }

    /**
     * A new instance of this fragment using the provided parameters.
     * @param text Parameter 1.
     * @return A new instance of fragment FourthFragment.
     */
    public static FourthFragment newInstance(String text) {
        FourthFragment fourthFragment = new FourthFragment();
        Bundle bundle = new Bundle();
        bundle.putString("msg", text);
        fourthFragment.setArguments(bundle);
        return fourthFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_third, container, false);
        TextView tvFragThird = (TextView) view.findViewById(R.id.tvFragThird);
        //Set text on fragment textview
        tvFragThird.setText(getArguments().getString("msg"));
        //On click display toast message
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Fragment 4", Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}