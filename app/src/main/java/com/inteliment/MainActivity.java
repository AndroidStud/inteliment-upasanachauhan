package com.inteliment;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.inteliment.test1.Test1Fragment;
import com.inteliment.test2.Test2Fragment;

/**
 * MainActivity is the base activity which binds every widget and functions accordingly.
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    FragmentManager fm;
    FrameLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        container = (FrameLayout) findViewById(R.id.maincontainer);
        fm = getSupportFragmentManager();
        Test1Fragment fragment = (Test1Fragment) fm.findFragmentByTag("tag_1");

        if (fragment == null) {
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.maincontainer, new Test1Fragment(), "tag_1");
            fragmentTransaction.commit();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.test1) {
            fm = getSupportFragmentManager();
            Test1Fragment fragment = (Test1Fragment) fm.findFragmentByTag("tag_1");

            if (fragment == null) {
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.maincontainer, new Test1Fragment(), "tag_1");
                fragmentTransaction.commit();
            }
        } else if (id == R.id.test2) {
            fm = getSupportFragmentManager();
            Test2Fragment fragment = (Test2Fragment) fm.findFragmentByTag("tag_2");

            if (fragment == null) {
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.maincontainer, new Test2Fragment(), "tag_2");
                fragmentTransaction.commit();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.mapContainer);
        if (fragment != null && fragment.isVisible()) {
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.maincontainer, new Test2Fragment(), "tag_2");
            fragmentTransaction.commit();
        } else {
            super.onBackPressed();
        }
    }
}