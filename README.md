Android Technical Evaluation
Here is a programming problem we ask you to read then create an Android app to solve it. For the solution, we request that you use Native Android Framework.	

A number of things will be assessed in this test including the design aspect of the solution and object oriented programming skills. You may use external libraries or tools for building or testing purposes.

Instructions

1. Refer to the instructions below for 2 Tests provided below. 
2. Develop the solutions & app accordingly 
3. Upload your test to Github (www.github.com) and share send link to contact details provided below.

Test 1:

1. Make five items with the same width to scroll horizontally
2. Make a Viewpager of four fragments. Each fragment should be clickable and should display Fragment Number in Toast
3. Display three TextViews in alignment as shown in above image
4. From point 1, click any item out of 5 and that item name should be populated in Point 4 as TextView.
5. Make three buttons of same size. For example, give button name, redBtn, blueBtn and greenBtn. On clicking any button, the app background should change as per button color name, which was clicked.







 
Test 2:

Sample Navigation App.

Point 1: Create a dropdown/spinner. Name should be fetched from JSON
Point 2: From JSON you will find transport information. Display transport info of relevant item selected in Spinner
Point 3: Each item has its own location. Clicking navigate button will display a Google map and locate latitude/longitude provided in JSON




A sample JSON file can be found at http://express-it.optusnet.com.au/sample.json

Notes:
•	Android Studio, gradle is suggested to make this mock app.
•	Design choice is up to developer whether they want to implement sliding menu or tab bar.
•	At least two flavor of app is required, RELEASE and DEBUG in build.gradle file.
o	ApplicationID should be different based on flavor selected in making build.
•	App should work both in Landscape and Portrait mode. Min SDK is up to developer to decide.